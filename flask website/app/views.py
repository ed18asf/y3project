from flask import render_template, redirect, request, session
from flask.helpers import url_for
from app import app
from app.forms import IndexForm
import time
import re

@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def index():
	print(request.form)

	session.clear()
	form = IndexForm()

	if request.method == "POST":
		if request.form.get('consent') == 'y':
			return redirect('/bot1')
	return render_template('index.html', title="Intro", form=form)

@app.route('/bot1', methods=['GET', 'POST'])
def bot1():
#embed chatbot1
	return render_template('bot1.html', title="bot1")

@app.route('/bot2', methods=['GET', 'POST'])
def bot2():
#embed chatbot2
	return render_template('bot2.html', title="bot2")

@app.route('/end', methods=['GET', 'POST'])
def end():
	#if not session.get('uid'):
	 #   return redirect('/index')
	session.clear()

	return render_template('end.html', title="Survey Time")
