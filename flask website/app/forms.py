from flask.app import Flask
from flask_wtf import FlaskForm
from wtforms.fields.simple import BooleanField

class IndexForm(FlaskForm):
	consent = BooleanField('consent')
