# Analysing User Sentiment Towards Emoji use in Chatbots for Education
**A final year project by Alain Fernando**
## The following open-source softwares have been used:
- Torchmoji by Thomas Wolf of Hugging Face (https://github.com/huggingface/torchMoji)
- The Flask micro web framework (https://github.com/pallets/flask)
- Botpress Open Source (https://github.com/botpress/botpress)
## Running locally on Windows
- Run bp.exe as found in the botpress folder
- In your browser of choice, navigate to either localhost:3000/s/emoji (for the emoji bot) or localhost:3000/s/proto2 (for the bot without emojis)

To view the website:

- In a terminal, use the command "flask run" in the flask website directory (provided the correct prerequisites are installed)
- To have the embedded Chatbots appear run bp.exe at the same time.
