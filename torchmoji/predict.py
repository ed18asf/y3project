from torchmoji.sentence_tokenizer import SentenceTokenizer
from torchmoji.model_def import torchmoji_emojis
import json, numpy

# Returns the indices of the k largest elements in array.
# top_elements code taken from example folder provided by torchMoji team
def top_elements(array, k):
        ind = numpy.argpartition(array, -k)[-k:]
        return ind[numpy.argsort(array[ind])][::-1]

def predict(text):
    vocab = json.load(open('model/vocabulary.json', 'r'))
    model = torchmoji_emojis('model/pytorch_model.bin')
    text = [text]
    tokens = SentenceTokenizer(vocab, 250).tokenize_sentences(text)[0]
    emoji = top_elements(model(tokens)[0], 1)
    return str(emoji[0])